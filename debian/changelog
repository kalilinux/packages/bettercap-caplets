bettercap-caplets (0+git20240106-2kali1) kali-dev; urgency=medium

  * Fix the patch to not use usr/local

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 10 Sep 2024 18:03:37 +0200

bettercap-caplets (0+git20240106-2) unstable; urgency=medium

  * bettercap-ui is not yet present on Debian, so let's keep this path.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Tue, 06 Aug 2024 16:21:12 +0000

bettercap-caplets (0+git20240106-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + bettercap-caplets: Add Multi-Arch: foreign.

  [ Francisco Vilmar Cardoso Ruviaro ]
  * New upstream version 0+git20240106.
  * Change 'Recommends: bettercap' to Depends.
  * Drop bettercap-ui from Depends field. (Closes: #1053331)
  * Refresh debian/patches/change-dir.patch.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Tue, 06 Aug 2024 14:37:22 +0000

bettercap-caplets (0+git20210429-1) unstable; urgency=medium

  * Upload to unstable.

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Wed, 05 Oct 2022 18:32:35 +0000

bettercap-caplets (0+git20210429-1~exp1) experimental; urgency=medium

  * Update debian/watch.
  * Update debian/rules.
  * Remove debian/kali-ci.yml.
  * Update debian/gbp.conf.
  * Update debian/copyright.
  * Update debian/control.
  * Run 'wrap-and-sort -a'.
  * Update debian/patches/change-dir.patch.
  * Update debian/patches/remove-makefile.patch.
  * Initial Debian Release. (Closes: #1020433)

 -- Francisco Vilmar Cardoso Ruviaro <vilmar@debian.org>  Wed, 21 Sep 2022 17:04:59 +0000

bettercap-caplets (0+git20210429-0kali1) kali-dev; urgency=medium

  * New upstream version 0+git20210429

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 25 May 2021 09:14:34 +0200

bettercap-caplets (0+git20210412-0kali1) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Wrap long lines in changelog entries: 0+git20201031-0kali1.

  [ Sophie Brun ]
  * Update debian/watch
  * New upstream version 0+git20210412

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 19 Apr 2021 13:56:01 +0200

bettercap-caplets (0+git20201031-0kali1) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Sophie Brun ]
  * New upstream version 0+git20201031
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 21 Jan 2021 16:30:42 +0100

bettercap-caplets (0+git20200413-0kali2) kali-dev; urgency=medium

  * Add bettercap-ui as depends for http-ui caplet (see 6596)

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 23 Jul 2020 10:15:54 +0200

bettercap-caplets (0+git20200413-0kali1) kali-dev; urgency=medium

  * New upstream version 0+git20200413
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 06 Jul 2020 15:07:52 +0200

bettercap-caplets (0+git20200110-0kali1) kali-dev; urgency=medium

  * New upstream version 0+git20200110

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 24 Feb 2020 14:01:21 +0100

bettercap-caplets (0+git20191009-0kali1) kali-dev; urgency=medium

  * New upstream version 0+git20191009
  * Update patch for new snapshot

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 29 Oct 2019 16:09:02 +0100

bettercap-caplets (0+git20190925-0kali1) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Add GitLab's CI configuration file
  * Configure git-buildpackage for Kali
  * Update URL in GitLab's CI configuration file

  [ Sophie Brun ]
  * New upstream version 0+git20190925
  * Refresh patch for new version
  * Bump Standards-Version to 4.4.0

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 27 Sep 2019 09:28:00 +0200

bettercap-caplets (0+git20190518-0kali1) kali-dev; urgency=medium

  * Add gbp.conf
  * New upstream version 0+git20190518
  * Refresh patch

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 25 Jun 2019 15:19:14 +0200

bettercap-caplets (0+git20190410-0kali1) kali-dev; urgency=medium

  * Import new snapshot

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 19 Apr 2019 10:22:04 +0200

bettercap-caplets (0+git20190330-0kali1) kali-dev; urgency=medium

  * Import new snapshot

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 02 Apr 2019 14:45:12 +0200

bettercap-caplets (0+git20190327-0kali1) kali-dev; urgency=medium

  * Import new snapshot

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 27 Mar 2019 15:18:21 +0100

bettercap-caplets (0+git20190303-0kali1) kali-dev; urgency=medium

  * Import new snapshot

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 12 Mar 2019 16:08:51 +0100

bettercap-caplets (0+git20190120-0kali1) kali-dev; urgency=medium

  * Initial release

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 20 Feb 2019 16:51:04 +0100
